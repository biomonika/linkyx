LinkYX

Fully automated pipeline for detection of sex linked genes using RNA-Seq data

The installation is described on the project wiki page:
https://bitbucket.org/biomonika/linkyx/wiki/Home

In order to install additional tools used in the workflow, please install following repository suite definitions to the Galaxy:
toolshed.g2.bx.psu.edu/repos/biomonika/suite_linkyx_bundle_main_0_1
testtoolshed.g2.bx.psu.edu/repos/biomonika/suite_linkyx_bundle_main_0_1

Code repositary: bitbucket.org/biomonika/linkyx

If you have any questions, please use following email address: biomonika@psu.edu